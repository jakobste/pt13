#include <iostream>
#include <cmath>
using namespace std;

double func(double x){
	double f=sin(x);
	return f;
}

int main (int argc, char * const argv[]) {
    // insert code here...
    cout << "Simpson Integration\n";
	double x0, x1, dx, nint;

	
	int step;
	
	cout << "x0:\n";
	cin >> x0;
	cout << "x1:\n";
	cin >> x1;
	
	cout << "number of steps:\n";
	cin >> step;
	
	double *dint=new double[step];
	
	dx= (double) (x1-x0)/step;
	
	for (int i=0; i<step; i++) {
		dint[i]=(dx/6)*(func(x0+i*dx)+4*func(x0+(i+0.5)*dx)+func(x0+(i+1)*dx));
	    nint+=dint[i];
	}
	
	cout << "The Valure for the Integral is:\n" << nint;
	delete[] dint;
    return 0;
}
