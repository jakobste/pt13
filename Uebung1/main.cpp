#include <iostream>
#include <cmath>
using namespace std;
int main () {
	cout << "Machine's floating point precision epsilon is:\n";
	
	double two=0.5;
	double a=1;
	for (int i=1; i<10000; i++) {
	    if (1+a!=1) {
	    	a=a-two;
	    }
		else {
			a=a+two;
		}

	    two=two/2;
	}
	cout << a;
	

    return 0;
}
