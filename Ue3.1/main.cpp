#include <iostream>
#include <cmath>
#include <string>

using namespace std;

int main (int argc, char * const argv[]) {
	
	/***************** Using Static Arrays ******************/
	
    double sum, arr[10], numin;
	int N;
	
	cout << "How many Numbers would you like to type in? [type in number from 1-10]\n";
	cin >> N;
	
	sum=0;
	
    cout << "type in " << N << " Numbers:\n"; 
	
	for (int i=0; i<N; i++) {
		cin >> numin;
		arr[i]=numin;
		sum+=numin; 
	} 
	
	cout << "Normalized Numbers in reverse order:\n";
	
	for (int i=0; i<N; i++) {
		cout << arr[N-1-i]/sum << "\n";
	}
	
	/***************** Using Dynamic Arrays ****************/
	
	cout << "How many Numbers would you like to type in?\n";
	cin >> N;
	
	double *arr2=new double[N];
	
	int j=0;
	sum=0;
	
    cout << "type in " << N << " Numbers:\n"; 
	
	for (int i=0; i<N; i++) {
		cin >> numin;
		arr2[i]=numin;
		sum+=numin; 
	} 
	
	cout << "Normalized Numbers in reverse order:\n";
	
	for (int i=0; i<N; i++) {
		cout << arr2[N-1-i]/sum << "\n";
	}
	
	delete[] arr2;
    return 0;
}
