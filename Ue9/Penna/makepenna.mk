CPP=c++
CPPFLAGS = -O2

all: a.out clean populationsize.m pop_plot.jpg

pop_plot.jpg: population_plot.m populationsize.m
	/Applications/MATLAB.app/bin/matlab -nodisplay -nosplash -r "population_plot"

	
populationsize.m: a.out
	./a.out

clean:
	rm pennamodel.o population.o animal.o genome.o
	
a.out: pennamodel.o population.o animal.o genome.o
	$(CPP) pennamodel.o population.o animal.o genome.o
	
all: genome.o animal.o population.o pennamodel.o 
	
pennamodel.o: pennamodel.cpp
		$(CPP) -c pennamodel.cpp
		
population.o: population.cpp
		$(CPP) -c population.cpp
		
animal.o: animal.cpp
		$(CPP) -c animal.cpp	
		
genome.o: genome.cpp
		$(CPP) -c genome.cpp				