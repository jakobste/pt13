#ifndef GENOME_HPP
#define GENOME_HPP

#include <bitset>
#include <limits>


namespace Penna
{

typedef unsigned age_type;

/**
 * Genome class. Each gene is represented by one bit.
 */
class Genome
{
public:
	friend class Population; // giving main() access to private variables
	
    /// Up to this size bitset is a lot faster
    static const age_type number_of_genes = 
        std::numeric_limits<unsigned long>::digits;
	
	typedef std::bitset<number_of_genes> gene_type;

    static void set_mutation_rate(const age_type);

    /// Default constructor: Initialize genes to all good.
    Genome() {};
	/// Constructor that inherits the genome of the "paren-animal"
	//Genome(gene_type);

    /// Check if i'th gene is bad.
    bool is_bad(const age_type) const;
    /// Count number of bad genes in first n years.
    age_type count_bad(const age_type) const;
    /// Generate a copy of this, except for M flipped genes.
    Genome mutate() const;

private:
    //typedef std::bitset<number_of_genes> gene_type;

    /// Parameter M in Penna's paper
    static age_type mutation_rate_;
    gene_type genes_;
};

} // end namespace Penna

#endif // !defined GENOME_HPP
