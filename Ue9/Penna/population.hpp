#ifndef POPULATION_H__
#define POPULATION_H__

#include "animal.hpp"
#include <list>

namespace Penna {
	
  class Population {
  public:
    static const age_type N_max; //why not initialise this in the constructor
		
    void time_step();
		
    void gene_analyse() const;
		
    Population();
		
    Population(const unsigned);
		
    unsigned pop_size() const;
  private: 
    std::list<Animal> l;
  };
	
	
}

#endif
