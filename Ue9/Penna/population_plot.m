N_pop=load('populationsize.m');
time=1:length(N_pop);
fig=figure('color','w')
plot(time,N_pop)
xlabel('Time')
ylabel('Population Size')
print(fig,'pop_plot.png')