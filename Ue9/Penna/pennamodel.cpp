#include <iostream>
#include "population.hpp"
#include <fstream>


using namespace Penna;
int main() {
	/*
	 age_type Genome::mutation_rate_ = 10;
	 age_type Animal::bad_threshold_ = 2;
	 age_type Animal::maturity_age_ = 10;
	 */
	Population testpop(4);
	
	std::ofstream output1;
	output1.open("populationsize.m");
	for (age_type i=0; i<1000; i++) {
		testpop.time_step();
		output1 << testpop.pop_size() << "\n";
	}

	testpop.gene_analyse();
	
	output1 << "\n\n";
	 
	output1.close();
	return 0;
}