#include "population.hpp"
#include <iostream>
#include <limits>
#include <bitset>

namespace Penna {
	
  const age_type Population::N_max=1000;
	
  void Population::time_step() {
    std::list<Animal>::iterator pos;
    Animal child;
		
    //check out std::for_each, std::transform and std::remove_if for
    //more elegance

    for (pos=l.begin(); pos!=l.end();++pos) {
			
      // remove with certain probability
      double rand_num= ((double) rand()/ (RAND_MAX));
			
      if ((rand_num<((double) l.size()/N_max))||(pos->is_dead())) {
	//std::cout << "# animals: " << l.size() << "erase animal:\n";
	l.erase(pos);
      }
      else {				
	// if animal is mature -> give birth to new animal
	if (pos->is_mature()) {
	  child = (pos)->give_birth();
	  l.push_front(child);
	  //std::cout << "animal gave birth!, # of animals:" << l.size();
	}
				
	(pos)->grow();
	//std::cout << "\n age: " << (pos)->age() << "\n";
	//++pos;
      }	
    }

  }
	
  void Population::gene_analyse() const{
    std::list<Animal>::const_iterator pos;
    // Animal child;
    std::bitset<Genome::number_of_genes> gene;
    int numbad[Genome::number_of_genes];
    for (int i=0; i<Genome::number_of_genes; ++i) {
      numbad[i]=0;
    }
    for (pos=l.begin(); pos!=l.end();++pos) {
      gene=(pos)->gen_.genes_;
      for (int i=0; i<Genome::number_of_genes; ++i) {
	numbad[i]+=(int) gene[i];
      }
    }
    for (int i=0; i<Genome::number_of_genes; ++i) {
      for(int j=0; j<numbad[i]; ++j) {
	std::cout << "x";
      }
      std::cout << std::endl;
      //std::cout << "\n age: " << (pos)->age() << "\n";				//++pos;
    }
		
  }
	
  Population::Population() 
    : l(1) {};
	
  Population::Population(const unsigned N)
    : l(N) {};
	
  unsigned Population::pop_size() const{
    return l.size();
  }
	
  //static unsigned int size() const ;
	
}
