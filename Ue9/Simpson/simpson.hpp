#ifndef SIMPSON_H__
#define SIMPSON_H__

#include <cassert>
#include <cstdlib>
#include <limits>

template <class F, class T, class U>
void simpson(const F& function, const T x0, const U x1, const unsigned Nbin, double &valret) {
  assert((x1>x0)&&(Nbin>2)); // Assertions
	
  //T *dint=new T[Nbin];
  double dx, valint;
	
  // Did not even need <limits> !!!  (???)
  /*
    if(std::numeric_limits<T>::is_integer) {
    std::cout << "hey" << std::endl;
    }
  */

  //see email for comments

  valint=0; //Value of the integral
  dx= (double) (x1-x0)/(Nbin-1); 
	
  for (unsigned i=0; i<(Nbin-2); i+=2) {
    valint+=(dx/3)*(function(i*dx)+4*function((i+1)*dx)+function((i+2)*dx));
  }
	
  valret=valint;
	
  //delete[] dint;
}

#endif
