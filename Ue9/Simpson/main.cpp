#include <iostream>
#include <cmath>
#include <cassert>
#include "simpson.hpp"

using namespace std;

// /*
struct testfun {

  testfun(const double l = 1.0)
  : lambda(l)
  {}

  double operator() (const double x) const{
    return sin(lambda*x);
  }
  double lambda;
};
 
// */



int main() {
  int N=5;
  double out;
  //double integrand[N];
  double lambda=0.1;
  testfun testobj(lambda);
  int x0=0;
  double x1=3.141/lambda;
  simpson(testobj,x0,x1,N,out);
  std::cout << out;
	std::cout << "check this change!\n";
}
