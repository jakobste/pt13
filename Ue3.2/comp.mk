CC=c++

a.out: main.cpp libintegrate.a
	$(CC) main.cpp -L. -lintegrate

libintegrate.a: simpson.o
	ar ruc libintegrate.a simpson.o

simpson.o: simpson.cpp
	$(CC) -c simpson.cpp


