#ifndef SIMPSON_H
#define SIMPSON_H

double simpson(double *integrand, double x0, double x1, int Nbin);
#endif