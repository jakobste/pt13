#include "simpson.h"
#include "assert.h"

double simpson(double *integrand, double x0, double x1, int Nbin) {
	//assert((integrand!=NULL)&&(x0!=NULL)&&(x1!=NULL)&&(Nbin!=NULL)); <- Did not work!
	
	double *dint=new double[Nbin];
	double valint, dx;

	valint=0; //Value of the integral
        dx= (double) 2*(x1-x0)/(Nbin-1); 
	
	for (int i=0; i<(Nbin-2); i+=2) {
		valint+=(dx/6)*(integrand[i]+4*integrand[i+1]+integrand[i+2]);
	}
	
	delete[] dint;
	return valint;	
}
