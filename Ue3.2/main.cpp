#include <iostream>
#include <cmath>
#include "simpson.h"

using namespace std;

int main() {
	double out, integrand[5];
	for (int i=0; i<5; i++) {
		integrand[i]=pow((double)i*0.5,2);
        }
	out=simpson(integrand,0,2,5);
	std::cout << out;
}
