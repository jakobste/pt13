#ifndef SIMPSON_H
#define SIMPSON_H

template <class F, class T>
void simpson(const F& function, const T x0, const T x1, int Nbin, T &valret);

#endif
