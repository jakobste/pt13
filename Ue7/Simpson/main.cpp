#include <iostream>
#include <cmath>
#include <cassert>
#include "simpson.h"

using namespace std;

// INTEGRATION ROUTINE WORKS WITH FUNCTION OBJ. / SIMPSON FUNCTION DIRECTLY DEFINED 
// IN THE MAIN FILE, HOWEVER MAIN.O DOES NOT LINK TO SIMPSON.O - ERROR MESSAGE:
/*
  Undefined symbols:
  "testfun::operator()(double)", referenced from:
  _main in main.o
  ld: symbol(s) not found
  collect2: ld returned 1 exit status
*/


// /*
struct testfun {

  testfun(const double l = 1.0)
  : lambda(l)
  {}

  double operator() (const double x) const{
    return sin(lambda*x);
  }
  double lambda;
};
 
// */



int main() {
  int N=5;
  double out;
  //double integrand[N];
  double lambda=0.1;
  testfun testobj(lambda);
  double x0=0;
  double x1=3.141/lambda;
  //cout << testobj(1) << "\n";
  simpson(testobj,x0,x1,N,out);
  //out=testobj(0.5);
  std::cout << out;
}
