#include "simpson.h"
#include <cassert>

template <class F, class T>
void simpson(const F& function, const T x0, const T x1, int Nbin, T &valret) {
  assert((x1>x0)&&(Nbin>2)); // Assertions
	
  //T *dint=new T[Nbin];
  T dx, valint;
	
  valint=0; //Value of the integral
  dx= (T) (x1-x0)/(Nbin-1); 
	
  for (T i=0; i<(Nbin-2); i+=2) {
    valint+=(dx/3)*(function(i*dx)+4*function((i+1)*dx)+function((i+2)*dx));
  }
	
  valret=valint;
	
  //delete[] dint;
}
