#ifndef TESTFUN_H
#define TESTFUN_H

struct testfun {
	double operator() (double x);
	double lambda;
};

#endif

/*
 *  testfun.h
 *  
 *
 *  Created by Jakob Steinbauer on 04.11.13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

