#include <iostream>
using namespace std;

class Testclass {
public:
	Testclass(int val) {
		somevalue=val;
	}
	
	static void set_stat (int inp) {
		teststat=inp;
	}
	int show_stat() {
		return teststat;
	}
	
	const Testclass reproduce() {
    Testclass newobj(somevalue-1);
    //newobj.somevalue=somevalue-1;
    return newobj;
    }
	int value() {
		return somevalue;
	}
	private:
	static int teststat;
    int somevalue;
};

int main() {
    Testclass obj(2);
	obj.set_stat(3);
	cout << obj.show_stat() << "\n";
    cout << obj.value() << "\n";
	Testclass newob=obj.reproduce();
	cout << newob.value() << "\n";
return 0;
}
