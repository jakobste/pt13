#include <iostream>
#include <string>
#include <cassert>
#include "genome.hpp"

namespace Penna
{
  age_type Genome::mutation_rate_ = 2;

  // constructor with genome input argument
  // Genome::Genome(gene_type gene) {
  //   genes_=gene;
  // } //class members are automatically copied
	
  void Genome::set_mutation_rate(const age_type m ) {
    mutation_rate_=m;
  }
	
  bool Genome::is_bad(const age_type age ) const
  {
    // return if gene corresponting to recent age is bad
    assert( age < genes_.size() );
    return genes_[age];
  }
	
  age_type Genome::count_bad(const age_type age ) const
  {
    return (genes_<<(number_of_genes-age-1)).count();
  }
	
  Genome Genome::mutate() const
  {
    Genome gene_new (*this);
    // flip up to M genes
    for (age_type i=0; i<mutation_rate_; ++i) { 
      const long rand_pos= rand() % number_of_genes;
      gene_new.genes_.flip(rand_pos);
    }
		
    return gene_new;
  }

	
}


/*
 *  genome.cpp
 *  
 *
 *  Created by Jakob Steinbauer on 31.10.13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

