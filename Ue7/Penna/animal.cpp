#include <iostream>
#include <cassert>
#include "animal.hpp"

namespace Penna
{
  age_type Animal::bad_threshold_ = 2;
  age_type Animal::maturity_age_ = 0;

  Animal::Animal()
    :   age_(0)
  {
  }

  void Animal::set_bad_threshold (const age_type t) {
    bad_threshold_= t;
  }
	
  void Animal::set_maturity_age (const age_type r ) {
    maturity_age= r;
  }
	
  Animal::Animal ( const Genome& gen )
    : gen_(gen)
    ,   age_(0)
  {}
	
  bool Animal::is_dead() const {
    return (age_ > maximum_age || gen_.count_bad(age_)>=bad_threshold_);
  }
	
  bool Animal::is_mature() const {
    return (age_>=maturity_age);
  }
	
  age_type Animal::age() {
    return age_;
  }
	
  void Animal::grow() {
    assert( !is_dead() );
    age_++;
  }
	
  Animal Animal::give_birth() const {
    assert( is_mature() );
    return Animal(gen_.mutate());
  }
}


/*
 *  animal.h
 *  
 *
 *  Created by Jakob Steinbauer on 03.11.13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

