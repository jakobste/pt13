#Programming Techniques Ex06

cmake_minimum_required(VERSION 2.8)

add_library(timer timer.cpp)
add_library(randomnumber random.cpp)
add_executable(benchmark main.cpp)
target_link_libraries(benchmark timer randomnumber)

install(TARGETS benchmark timer randomnumber
	RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib
)