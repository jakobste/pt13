#ifndef GENOME_H__
#define GENOME_H__

#include <bitset>

class Genome  
{

public:

  static const unsigned max_age = 64;

  Genome();
  
private:

  std::bitset<max_age> genes;
};

#endif



