#include "timer.hpp"

namespace progtech{

void timer::start() {
  gettimeofday(&t0,NULL);
}

void timer::stop() {
  gettimeofday(&t1,NULL);
}

  double timer::duration() const{
  return (t1.tv_sec - t0.tv_sec)+(t1.tv_usec - t0.tv_usec) / 1e06;
}
  
}

/*
 *  timer.h
 *  
 *
 *  Created by Jakob Steinbauer on 28.10.13.
 *  Copyright 2013 __MyCompanyName__. All rights reserved.
 *
 */

