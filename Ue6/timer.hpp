#ifndef TIMER_H__
#define TIMER_H__

#include <sys/time.h>
#include <cstddef>

namespace progtech{

class timer
{
public:
  void start();
  void stop();
  double duration() const;
  
private:
  
    timeval t0, t1;
};

}

#endif
