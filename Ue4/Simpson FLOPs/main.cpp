#include <iostream>
#include <cmath>
#include "simpson.h"
#include <sys/time.h>

using namespace std;

int main() {
  //int N=5;
  double out, time;
  for (int N=1; N<1e05;N+=100) {
    double integrand[N];
    for (int i=0; i<N; i++) {
      //integrand[i]=pow((double)i*2/(N-1),2); //the pow function is
      //not very fast, even if you dont time it
      const double tmp = i*2.0/(N-1);
      integrand[i] = tmp*tmp;
    }
	
    timeval t0, t1;
	
    simpson(integrand,0,2,N,time,out); //why not out = simpson(integrand,0,2,N,time) ?
    int fpn=3*(N-1)+3; //only 3 operations per interval? should be in the order of 10
    std::cout << (int) fpn/time << "\n ";
  }
  //std::cout << out;
}
