CC=g++ -O3 #enable optimisation

flop.jpg: flop.m MFlops.m
	/Applications/MATLAB.app/bin/matlab -nodisplay -nosplash -r "flop.m"

MFlops.m: a.out 
	./a.out > MFlops.m

a.out: main.cpp libintegrate.a
	$(CC) main.cpp -L. -lintegrate

libintegrate.a: simpson.o
	ar ruc libintegrate.a simpson.o

simpson.o: simpson.cpp
	$(CC) -c simpson.cpp

