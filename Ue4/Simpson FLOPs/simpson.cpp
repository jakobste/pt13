#include "simpson.h"
#include <cassert>
#include <sys/time.h>

void simpson(double *integrand, double x0, double x1, int Nbin,double &time, double &valret) {
  assert(Nbin>2); //<- Did not work!
  assert(x1>x0);
  	
  // double *dint=new double[Nbin]; //why do you allocate this?
  double dx, valint;
  timeval t0, t1;
	
  gettimeofday(&t0,0);
  valint=0; //Value of the integral
  dx= (double) 2*(x1-x0)/(Nbin-1); 
	
  for (int i=0; i<(Nbin-2); i+=2) {
    valint+=(dx/6)*(integrand[i]+4*integrand[i+1]+integrand[i+2]);
  }
  gettimeofday(&t1,0);
  time=(double) t1.tv_usec-t0.tv_usec;

  if (time==0) //whats this? why?
    time=1;
	
  valret=valint;
	
  // delete[] dint;
  //return valint;	
}
