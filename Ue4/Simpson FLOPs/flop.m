flops=load('MFlops.m');
x=(1:size(flops,1))*100-97;
figure('color','w')
plot(x,flops)
xlabel('Number of bins')
ylabel('Number of mega FLOP/s')
