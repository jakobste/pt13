#ifndef SIMPSON_H
#define SIMPSON_H

void simpson(double *integrand, double x0, double x1, int Nbin,double &time, double &valret);
#endif