#include <iostream>
#include <sys/time.h>

int main() {
  int nth, Nit, N;
  double time;
  Nit=20; //increase number of steps
  timeval t0, t1;
  for (int k=1; k<100; k++) { // Arraysize
    N=10000*k;
    double arr[N];
    for (int j=0; j<N; j++) {
      arr[j]=0;
    }
    for (int j=1; j<=Nit; j++) { // Variation of the stepsize n
      nth=1000+10*j;
			
      time=0;
      //you may need many more measurements than 10 for stable results
      for (int m=0;m<10;m++) { // taking the average of 10 measurements
	gettimeofday(&t0,0);
	for (int i=0; i<N; i+=nth) {
	  arr[i]++;
	}
		
	gettimeofday(&t1,0);
	time+=(t1.tv_usec-t0.tv_usec);
      }
      time= (double) time/10;

      if (!(time>0.)) //again, why?
	time=1;

      std::cout << (double) N/(nth*time) << " ";
    }
    std::cout  << "\n";
  }
  return 0;
}
