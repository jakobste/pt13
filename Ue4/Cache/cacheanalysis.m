dat=load('cache.m');
k=10000*(1:99);
N=20;
fig=figure;
for i=1:N,
plot(k,dat(:,i));
hold on
end
xlabel('Array size')
ylabel('mega Flops/s')
print(fig,'cachesize.eps')