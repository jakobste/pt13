CC=g++ -O3 -funroll-loops #compiler optimizations and loop unrolling

cachesize.eps: cacheanalysis.m cache.m
	/Applications/MATLAB.app/bin/matlab -nodisplay -nosplash -r "cacheanalysis"

cache.m: a.out 
	./a.out > cache.m

a.out: cache.cpp
	$(CC) cache.cpp
